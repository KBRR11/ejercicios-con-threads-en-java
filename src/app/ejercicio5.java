package app;

import java.util.ArrayList;
import java.util.Scanner;

public class ejercicio5 extends Thread {
    Scanner nota = new Scanner(System.in);
    ArrayList<Double> notas = new ArrayList<>();
double value;
    public ejercicio5(){
        System.out.println("**** SISTEMA DE PROMEDIO ****");
for (int i = 0; i < 5; i++) {
    System.out.println("Ingresar nota ("+(i+1)+"/5): ");
    value= nota.nextDouble();
    notas.add(value);
    //System.out.println(value +" en la posicion "+ i);
}
    }
    @Override
    public void run() {
        double suma = notas.get(0)+notas.get(1)+notas.get(2)+notas.get(3)+notas.get(4);
        System.out.println("\n\nLa Suma de las notas es:"+suma);
        double average = suma/5;
        System.out.println("\n\nEl Promedio es: "+average);
        System.out.println("\n----------------------------------\n\n");
    }

    public static void main(String[] args)throws Exception {
        while (true) {
            Thread promedio = new ejercicio5();
            promedio.start();
            promedio.sleep(2000);
        }

    }
}