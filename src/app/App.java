package app;

import java.util.Scanner;

public class App  extends Thread{

     Scanner dato = new Scanner(System.in); 
     String nombre, dia;
     double hora;

    public App(String nombre, String dia , double hora) {
        this.nombre = nombre;
        this.dia= dia;
        this.hora= hora;
        System.out.println("Ingresar Nombre: ");
        nombre = dato.nextLine();
        System.out.println("Ingresar día: ");
        dia = dato.nextLine();
        System.out.println("Ingresar hora: ");
        hora = dato.nextDouble();
    }
    @Override
    public void run(){
        if(hora>13){
            System.out.println(nombre+" Ha llegado Tarde el día " + dia);
        }else{
            System.out.println(nombre +" Ha llegado Temprano el día " + dia);
        }
    }
    public static void main(String[] args) throws Exception {
       Thread alum1 = new App("Keny", "Lunes", 12);
       alum1.start();
       Thread.sleep(3000);
       Thread alum2 = new App("Pedro", "Lunes", 14);
       alum2.start();
       Thread.sleep(3000);

    }
    
}