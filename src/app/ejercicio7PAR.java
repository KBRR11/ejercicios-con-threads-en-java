package app;

public class ejercicio7PAR extends Thread{
    
    public ejercicio7PAR() {
        System.out.println("------- NUMEROS PARES -----");
        
    }
    
    @Override
    public void run(){

        int n;
        int contador=0;
        for (n=1;n<=15;n++){ 
            if (n % 2 == 0) {
            System.out.print(" "+n+"\n");
            contador=contador+n;
            } 
         }
        System.out.print("La suma de los pares es : "+contador + "\n");
        
    } 
   
}
