package app;

import java.util.*;

public class ejercicio6 extends Thread{
    Scanner estacion = new Scanner(System.in);
    /*ArrayList<String> meses = new ArrayList<>();*/
  private String  meses [] = {"septiembre, octubre, noviembre","diciembre, enero, febrero","marzo, abril, mayo","junio, julio, agosto"} ; 
   int value;
    public ejercicio6(){
        System.out.println("---- ESTACIONES DEL AÑO ----");
        System.out.println(" 1. Primavera");
        System.out.println(" 2. Verano");
        System.out.println(" 3. Otoño");
        System.out.println(" 4. Invierno ");
        System.out.println("-------------------");
        System.out.print("Selecciona ítem para mirar meses: ");
         value = estacion.nextInt();
    }
    @Override
    public void run() {
        System.out.println(meses[value-1]+"\n\n");
    }
    public static void main(String[] args)throws Exception {
        while (true) {
            Thread estaciones = new ejercicio6();
            estaciones.start();
            estaciones.sleep(2000);
        }
    }
       
}